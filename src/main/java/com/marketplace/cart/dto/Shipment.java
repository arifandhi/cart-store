package com.marketplace.cart.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Shipment {
    private String provider;
    private String jenis;
    private String asal;
    private String tujuan;
    private BigDecimal berat;
    private BigDecimal biaya;
}
