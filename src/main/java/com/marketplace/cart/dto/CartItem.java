package com.marketplace.cart.dto;

import lombok.Data;

@Data
public class CartItem {
    private Product product;
    private Integer jumlah;
}