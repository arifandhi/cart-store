package com.marketplace.cart.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class PurchaseOrder {
    private Date waktuTransaksi = new Date();
    private List<CartItem> daftarBelanja = new ArrayList<>();
    private Shipment pengiriman = new Shipment();
}
